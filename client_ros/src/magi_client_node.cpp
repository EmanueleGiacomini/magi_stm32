// #include <cv_bridge/cv_bridge.h>
#include <encoder_packets.h>
#include <math.h>
#include <ros/package.h>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <iostream>

#include <client.hpp>
// #include <opencv4/opencv2/core.hpp>

#include "manta_listener.hpp"
#include "ros.hpp"

const std::string TOPIC_JOINT = "/magi/joint_state";
const std::string TOPIC_CAMERA0 = "/camera0/image_raw";
const std::string TOPIC_CAMERA1 = "/camera1/image_raw";

const std::string camera0_id("000F314CE570");
const std::string camera1_id("000F314CE573");

std::string camera0_params, camera1_params;

const std::string DEVICE_NAME = "/dev/ttyACM0";
const int BAUDRATE = 115200;

ros::Publisher joint_pub;

// short int prev_measures[encoder::MaxEncoders];
// sensor_msgs::JointState joint_msg;

// void encoder_packet_callback(encoder::PacketEncoder* packet) {
//   joint_msg.name.resize(encoder::MaxEncoders);
//   joint_msg.position.resize(encoder::MaxEncoders);
//   joint_msg.velocity.resize(encoder::MaxEncoders);
//   joint_msg.effort.resize(encoder::MaxEncoders);
//   for (int i = 0; encoder::MaxEncoders; ++i) {
//     joint_msg.header.stamp = ros::Time::now();
//     joint_msg.name[i] = std::to_string(i);
//     if (i == 0) {
//       joint_msg.position[i] = packet->measurements[i] / 4095 * 2 * M_PI;
//     } else {
//       joint_msg.position[i] =
//           (packet->measurements[i] % 5000) / 5000 * 2 * M_PI;
//     }
//     joint_msg.velocity[i] = packet->measurements[i] - prev_measures[i];
//     prev_measures[i] = packet->measurements[i];
//   }
//   joint_pub.publish(joint_msg);
// }

int main(int argc, char* argv[]) {
  ros::init(argc, argv, "magi_client_node");
  ros::NodeHandle nh;

  nh.param<std::string>(
      "camera0/params_file", camera0_params,
      ros::package::getPath("client_ros") + "/configs/000F314CE570.xml");
  nh.param<std::string>(
      "camera1/params_file", camera1_params,
      ros::package::getPath("client_ros") + "/configs/000F314CE573.xml");

  std::cerr << camera0_params << std::endl;

  ros::Publisher camera0_pub =
      nh.advertise<sensor_msgs::Image>(TOPIC_CAMERA0, 30);
  MantaListener camera0(camera0_id.c_str(), 3);
  camera0.setParams(camera0_params);
  camera0.setCallback([&](const Vimba::FramePtr frame) {
    unsigned int img_w, img_h;
    uint8_t* img_ptr;

    frame->GetHeight(img_h);
    frame->GetWidth(img_w);
    frame->GetBuffer(img_ptr);

    sensor_msgs::Image msg;

    srrg2::sbdg::ros::mono2rosimg(img_ptr, msg, img_h, img_w);
    msg.header.stamp = srrg2::sbdg::ros::TimeStamp::now().toRosTime();
    camera0_pub.publish(msg);
  });
  ros::Publisher camera1_pub =
      nh.advertise<sensor_msgs::Image>(TOPIC_CAMERA1, 30);
  MantaListener camera1(camera1_id.c_str(), 3);
  camera1.setParams(camera1_params);
  camera1.setCallback([&](const Vimba::FramePtr frame) {
    unsigned int img_w, img_h;
    uint8_t* img_ptr;

    frame->GetHeight(img_h);
    frame->GetWidth(img_w);
    frame->GetBuffer(img_ptr);

    // cv::Mat cv_img(img_h, img_w, CV_8UC1, img_ptr);
    sensor_msgs::Image msg;
    srrg2::sbdg::ros::mono2rosimg(img_ptr, msg, img_h, img_w);
    msg.header.stamp = srrg2::sbdg::ros::TimeStamp::now().toRosTime();
    camera1_pub.publish(msg);
    // std_msgs::Header msg_header;
    // sensor_msgs::ImagePtr msg_ptr =
    //     cv_bridge::CvImage(msg_header, "bayer_rggb8", cv_img).toImageMsg();
    // msg = *msg_ptr;
    // Correct asap
    // msg.header.stamp = srrg2::sbdg::ros::TimeStamp::now().toRosTime();
    // msg.encoding = "bayer_rggb8";
    // Publish TODO
    // camera1_pub.publish(msg);
  });

  joint_pub = nh.advertise<sensor_msgs::JointState>(TOPIC_JOINT, 10);

  // Client client(DEVICE_NAME, BAUDRATE);
  // PacketTraitsEncoder encoder_traits(*(client.getParser()), client);
  // client.installCallback(encoder_packet_callback);

  //camera0.startAcquiring();
  camera1.startAcquiring();
  ros::spin();
  // while (ros::ok()) {
  //   ros::spinOnce();
  // }

  return 0;
}
