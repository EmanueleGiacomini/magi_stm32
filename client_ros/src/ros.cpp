/**
 * @file ros.cpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2021-11-17
 *
 * @copyright Copyright (c) 2021
 *
 */

#include "ros.hpp"

#include <sensor_msgs/image_encodings.h>
#include <std_msgs/Header.h>

#include <chrono>

namespace srrg2 {
namespace sbdg {
namespace ros {
TimeStamp TimeStamp::now(void) {
  using namespace std;
  TimeStamp ts;
  auto uts = chrono::high_resolution_clock::now().time_since_epoch();
  ts.sec = chrono::duration_cast<chrono::seconds>(uts).count();
  ts.nsec =
      chrono::duration_cast<chrono::nanoseconds>(uts).count() % 1000000000;
  return ts;
}

void mono2rosimg(const uint8_t *src_, sensor_msgs::Image &dst_, uint32_t h_,
                 uint32_t w_) {
  dst_.height = h_;
  dst_.width = w_;
  dst_.encoding = sensor_msgs::image_encodings::MONO8;
  dst_.is_bigendian = false;
  dst_.step = sizeof(uint8_t) * w_;
  dst_.data.resize(h_ * dst_.step);
  memcpy(dst_.data.data(), src_, h_ * dst_.step);
}
}  // namespace ros
}  // namespace sbdg
}  // namespace srrg2