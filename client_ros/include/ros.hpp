/**
 * @file ros.hpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2021-11-17
 *
 * @copyright Copyright (c) 2021
 *
 */

#pragma once
#include <ros/time.h>
#include <sensor_msgs/Image.h>

#include <cstdint>
#include <mutex>
#include <queue>

using ros::Time;

namespace srrg2 {
namespace sbdg {
namespace ros {

/**
 * @brief TimeStamp encapsulates a ros compliant timestamp generator
 *
 */
struct TimeStamp {
  uint64_t sec;
  uint64_t nsec;
  static TimeStamp now(void);
  inline Time toRosTime(void) {
    Time t_ros;
    t_ros.sec = sec;
    t_ros.nsec = nsec;
    return t_ros;
  }
};

/**
 * @brief Conversion from 8UC1 raw image to a ROS sensor_msgs::Image message.
 *
 * @param src_ pointer to the raw 8UC1 image
 * @param dst_ reference to the message to be written
 * @param h_ heigth of the raw image
 * @param w_ width of the raw image
 */
void mono2rosimg(const uint8_t *src_, sensor_msgs::Image &dst_, uint32_t h_,
                 uint32_t w_);

template <typename T>
struct BagEntry {
  std::string _topic;
  Time _tv;
  T msg;
};

// https://codetrips.com/2020/07/26/modern-c-writing-a-thread-safe-queue/
template <typename T>
class Queue {
 public:
  Queue() = default;
  Queue(const Queue<T> &) = delete;
  Queue &operator=(const Queue<T> &) = delete;

  Queue(Queue<T> &&other) {
    std::lock_guard<std::mutex> lock(_mutex);
    _queue = std::move(other._queue);
  }

  virtual ~Queue() {}

  inline unsigned long size() const {
    std::lock_guard<std::mutex> lock(_mutex);
    return _queue.size();
  }

  std::optional<T> pop() {
    std::lock_guard<std::mutex> lock(_mutex);
    if (_queue.empty()) return {};
    T tmp = _queue.front();
    _queue.pop();
    return tmp;
  }

  void push(const T &item) {
    std::lock_guard<std::mutex> lock(_mutex);
    _queue.push(item);
  }

  void clear(void) {
    std::lock_guard<std::mutex> lock(_mutex);
    std::queue<T> empty;
    std::swap(_queue, empty);
  }

 protected:
  std::queue<T> _queue;
  std::mutex _mutex;
  bool empty() const { return _queue.empty(); }
};
}  // namespace ros
}  // namespace sbdg
}  // namespace srrg2