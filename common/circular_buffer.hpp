/**
 * @file circular_buffer.hpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2022-05-23
 *
 * @copyright Copyright (c) 2022
 *
 */
#pragma once
#include <stdint.h>
#include <string.h>

template <typename DataType, uint16_t BufSize>
class circular_buffer {
 public:
  explicit circular_buffer()
      : _max_size(BufSize), _head(0), _tail(0), _full(false) {
    memset((uint8_t*)_buf, 0x0, BufSize * sizeof(DataType));
  }

  inline void reset() {
    _head = _tail;
    _full = false;
  }

  inline bool empty() const { return !_full && (_head == _tail); }
  inline bool full() const { return _full; }
  inline uint16_t capacity() const { return BufSize; }
  inline uint16_t size() const {
    uint8_t size = BufSize;
    if (!_full) {
      if (_head >= _tail) {
        size = _head - _tail;
      } else {
        size = _tail - _head;
      }
    }
    return size;
  }

  inline void put(DataType src) {
    _buf[_head] = src;
    if (_full) {
      _tail = (_tail + 1) % BufSize;
    }
    _head = (_head + 1) % BufSize;
    _full = _head == _tail;
  }

  inline DataType get(void) {
    if (empty()) {
      return DataType();
    }
    DataType item = _buf[_tail];
    _full = false;
    _tail = (_tail + 1) % BufSize;
    return item;
  }

  inline DataType getMean(void) {
    if (empty()) {
      return DataType();
    }
    DataType mean = 0;
    int i = 0;
    while (i < size()) {
      i = (i + 1) % BufSize;
      mean += _buf[_head + i];
    }
    return mean / size();
  }

 protected:
  uint16_t _max_size;
  uint16_t _head = 0;
  uint16_t _tail = 0;
  bool _full;
  DataType _buf[BufSize];
};