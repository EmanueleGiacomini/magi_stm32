/**
 * @file packet_traits.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2022-05-22
 *
 * @copyright Copyright (c) 2022
 *
 */

#pragma once

#include <stdint.h>

using PacketSize = uint8_t;
using PacketId = uint8_t;
using PacketIndex = uint8_t;
using PacketSeq = uint8_t;
using PacketEpoch = uint16_t;

static constexpr uint8_t RespOk = 0x0;
static constexpr uint8_t RespFail = 0x1;
static constexpr uint8_t CmdGet = 0x2;
static constexpr uint8_t CmdFetch = 0x3;
static constexpr uint8_t CmdStore = 0x4;  // Not implemented

using CommandType = uint8_t;

// Templetized PacketId
template <typename PacketType_>
constexpr PacketId PacketId_ = -1;
constexpr int MaxCmdBuffer = 16;

class PacketParser;

struct PacketTraitsBase {
  PacketTraitsBase(PacketParser &parser_, PacketId id_, PacketSize size_,
                   PacketIndex max_idx_, uint8_t *temp_buffer_);
  PacketParser &_parser;
  const PacketId _id;
  const PacketSize _size;
  const PacketIndex _max_idx;
  uint8_t *_temp_buffer;
  virtual CommandType handleCommand(CommandType cmd_, PacketIndex idx_) {
    return RespFail;
  }
  virtual void rxComplete(PacketIndex, PacketSeq) = 0;
};

template <typename PayloadType_, PacketSize MaxIdx_>
struct PacketTraits_ : PacketTraitsBase {
  using PayloadType = PayloadType_;
  static constexpr PacketId Id = PacketId_<PayloadType>;
  static constexpr PacketSize MaxIdx = MaxIdx_;

  PayloadType _temp_buffer;
  PayloadType *_buffers[MaxIdx];

  PacketTraits_(PacketParser &parser_);

  void installBuffer(PacketIndex idx_, PayloadType &payload_);
  void removeBuffer(PacketIndex idx_);

  void rxComplete(PacketIndex, PacketSeq) override;
};

#include "packet_traits_impl.tpp"