/**
 * @file packet_traits.cpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2022-05-23
 *
 * @copyright Copyright (c) 2022
 *
 */
#include "packet_traits.h"

#include "packet_parser.h"

PacketTraitsBase::PacketTraitsBase(PacketParser& parser_, PacketId id_,
                                   PacketSize size_, PacketIndex max_idx_,
                                   uint8_t* temp_buffer_)
    : _parser(parser_),
      _id(id_),
      _size(size_),
      _max_idx(max_idx_),
      _temp_buffer(temp_buffer_) {
  _parser._traits[_id] = this;
}

#ifdef __CLIENT__
PacketTraitsBase::~PacketTraitsBase() { _parser._traits[_id] = 0; }
#endif
