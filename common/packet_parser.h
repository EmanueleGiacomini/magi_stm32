/**
 * @file packet_parser.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2022-05-22
 *
 * @copyright Copyright (c) 2022
 *
 */

#pragma once
#include <functional>

#include "packet_traits.h"

class PacketParser {
 public:
  static constexpr PacketId MaxPacketId = 16;
  PacketTraitsBase* _traits[MaxPacketId];
  friend class PacketTraitsBase;
  enum Status {
    Unsync = 0,
    Sync = 1,
    PId = 2,
    Seq = 3,
    Index = 4,
    Payload = 5,
    Checksum = 6
  };
  PacketParser();
  inline void parseChar(uint8_t);

  template <typename PayloadType_>
  bool sendPacket(const PayloadType_&, PacketIndex = 0);

  inline PacketSeq currentRxSeq(void) const { return _current_rx_seq; }
  inline uint16_t rxPacketCount(void) const { return _rx_packet_count; }
  inline uint16_t txPacketCount(void) const { return _tx_packet_count; }

  using TxCallback = std::function<bool(uint8_t)>;
  inline void setTxCallback(TxCallback cb_) { _tx_cb = cb_; }

 protected:
  PacketEpoch _epoch = 0;
  uint8_t* _buffer = 0;
  uint8_t* _buffer_end = 0;
  Status _status = Unsync;
  PacketIndex _current_index = 0;
  PacketTraitsBase* _current_trait = nullptr;
  PacketSeq _current_rx_seq = 0, _current_tx_seq = 0;
  uint16_t _rx_packet_count = 0;
  uint16_t _tx_packet_count = 0;
  TxCallback _tx_cb = nullptr;
};

#include "packet_parser_impl.tpp"