/**
 * @file packet_traits_impl.cpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2022-05-22
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "packet_traits.h"

// TODO

template <typename PayloadType_, PacketSize MaxIdx_>
PacketTraits_<PayloadType_, MaxIdx_>::PacketTraits_(PacketParser& parser_)
    : PacketTraitsBase(parser_, Id, sizeof(PayloadType), MaxIdx,
                       (uint8_t*)&_temp_buffer) {
  for (uint8_t i = 0; i < MaxIdx; ++i) {
    _buffers[i] = nullptr;
  }
}

template <typename PayloadType_, PacketSize MaxIdx_>
void PacketTraits_<PayloadType_, MaxIdx_>::installBuffer(
    PacketIndex idx_, PayloadType& payload_) {
  _buffers[idx_] = &payload_;
}

template <typename PayloadType_, PacketSize MaxIdx_>
void PacketTraits_<PayloadType_, MaxIdx_>::removeBuffer(PacketIndex idx_) {
  _buffers[idx_] = nullptr;
}

template <typename PayloadType_, PacketSize MaxIdx_>
void PacketTraits_<PayloadType_, MaxIdx_>::rxComplete(PacketIndex idx_,
                                                      PacketSeq seq_) {
  if (idx_ >= MaxIdx) return;
  if (_buffers[idx_] == nullptr) return;
  *(_buffers[idx_]) = _temp_buffer;
}
