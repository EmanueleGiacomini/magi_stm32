/**
 * @file encoder_packets.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2022-05-23
 *
 * @copyright Copyright (c) 2022
 *
 */

#pragma once

#include "packet_traits.h"

namespace encoder {
using EncoderMeasure = uint16_t;
constexpr uint8_t MaxEncoders = 6;

#pragma pack(push, 1)
struct PacketEncoder {
  uint8_t camera_sync;
  EncoderMeasure measurements[MaxEncoders];
};
#pragma pack(pop)

}  // namespace encoder

template <>
constexpr PacketId PacketId_<encoder::PacketEncoder> = 1;
