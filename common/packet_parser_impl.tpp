/**
 * @file packet_parser_impl.cpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2022-05-23
 *
 * @copyright Copyright (c) 2022
 *
 */

#pragma once
#include "packet_parser.h"

void PacketParser::parseChar(uint8_t c) {
  switch (_status) {
    case Unsync:  // 0
      if (c == 0xAA) _status = Sync;
      break;
    case Sync:  // 1
      if (c == 0x55)
        _status = PId;
      else
        _status = Unsync;
      break;
    case PId:  // 2
      if (c <= MaxPacketId) _current_trait = _traits[c];
      if (_current_trait)
        _status = Seq;
      else
        _status = Unsync;
      break;
    case Seq:  // 3
      _current_rx_seq = c;
      _status = Index;
      break;
    case Index:  // 4
      if (c >= _current_trait->_max_idx) {
        _status = Unsync;
        _current_trait = 0;
        _current_index = 0;
        _buffer = 0;
      } else {
        _current_index = c;
        _status = Payload;
        _buffer = _current_trait->_temp_buffer;
        _buffer_end = _buffer + _current_trait->_size;
      }
      break;
    case Payload:  // 5
      *_buffer = c;
      ++_buffer;
      if (_buffer == _buffer_end) _status = Checksum;
      break;
    case Checksum:  // 6
      uint8_t v = _current_index ^ _current_rx_seq;
      _buffer = _current_trait->_temp_buffer;
      while (_buffer != _buffer_end) {
        v ^= (*_buffer);
        ++_buffer;
      }
      if (c == v) {
        _current_trait->rxComplete(_current_index, _current_rx_seq);
        ++_rx_packet_count;
      }
      _status = Unsync;
      _current_index = 0;
      _current_trait = 0;
      _buffer = 0;
      _buffer_end = 0;
  }
}

template <typename PayloadType_>
bool PacketParser::sendPacket(const PayloadType_& payload, PacketIndex idx) {
  constexpr PacketId id = PacketId_<PayloadType_>;
  if (id >= MaxPacketId) return false;
  if (!_tx_cb(0xAA)) return false;
  if (!_tx_cb(0x55)) return false;
  if (!_tx_cb(id)) {
    return false;
  }
  if (!_tx_cb(_current_tx_seq)) return false;
  if (!_tx_cb(idx)) return false;
  uint8_t checksum = idx ^ _current_tx_seq;
  const uint8_t* b = (const uint8_t*)&payload;
  const uint8_t* b_end = b + sizeof(PayloadType_);
  while (b != b_end) {
    if (!_tx_cb(*b)) return false;
    checksum ^= *b;
    ++b;
  }
  if (!_tx_cb(checksum)) return false;
  ++_current_tx_seq;
  ++_tx_packet_count;
  return true;
}
