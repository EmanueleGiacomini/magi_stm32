/**
 * @file fw_main.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2022-05-20
 *
 * @copyright Copyright (c) 2022
 *
 */

#pragma once
#include <stdint.h>

#include "main.h"

#ifdef __cplusplus
extern "C" {
#endif

extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;
extern TIM_HandleTypeDef htim11;
extern TIM_HandleTypeDef htim10;

extern UART_HandleTypeDef huart2;

void fw_main(void);

#ifdef __cplusplus
}
#endif