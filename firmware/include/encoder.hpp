/**
 * @file encoder.hpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2022-05-20
 *
 * @copyright Copyright (c) 2022
 *
 */

#pragma once

#include <stdint.h>

#include <functional>

#include "fw_main.h"

class EncoderInterface {
 public:
  EncoderInterface(void) {}
  virtual void sample(void) = 0;
  inline uint16_t sampled_value(void) { return _sampled_value; }

 protected:
  uint16_t _sampled_value;  // Sampled measurement
};

class AbsoluteEncoder : public EncoderInterface {
 public:
  AbsoluteEncoder(GPIO_TypeDef *port_, uint16_t pin_, uint16_t exti_line_,
                  TIM_HandleTypeDef *htim_);
  void sample(void) override;
  void proc_isr(uint16_t exti);
  void spin(void);
  inline uint8_t &ready(void) { return _ready; }

  using ProtocolStartCb = std::function<void(void)>;
  inline void set_protocol_start_cb(ProtocolStartCb fn_) {
    _protocol_start_cb = fn_;
  }

  // TODO
  GPIO_TypeDef *_port;
  uint16_t _pin, _exti_line;
  uint16_t _inter_value;  // Intermediate measurement
  TIM_HandleTypeDef *_htim;
  // SPI mandracata vars
  uint8_t _ready = false;
  uint8_t _bit_idx = 0;

 protected:
  ProtocolStartCb _protocol_start_cb = nullptr;
};

class IncrementalEncoder : public EncoderInterface {
 public:
  IncrementalEncoder(TIM_HandleTypeDef *htim_);
  void sample(void);

 protected:
  TIM_HandleTypeDef *_htim;
  // TODO
};