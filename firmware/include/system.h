/**
 * @file system.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2022-05-23
 *
 * @copyright Copyright (c) 2022
 *
 */

#pragma once

#include "encoder.hpp"
#include "encoder_packets.h"

class EncoderManager {
 public:
  EncoderManager() {
    for (uint8_t i = 0; i < encoder::MaxEncoders; ++i) _encoders[i] = nullptr;
  };

  inline void sample(void) {
    for (uint8_t i = 0; i < encoder::MaxEncoders; ++i) {
      EncoderInterface* enc = _encoders[i];
      if (!enc) continue;
      enc->sample();
      _reading.measurements[i];
    }
  }

  inline void registerEncoder(EncoderInterface* interface_, uint8_t enc_idx_) {
    if (enc_idx_ >= encoder::MaxEncoders) return;
    _encoders[enc_idx_] = interface_;
  }

  encoder::PacketEncoder& data(void) { return _reading; }

 protected:
  EncoderInterface* _encoders[encoder::MaxEncoders];
  encoder::PacketEncoder _reading;
};

class MantaTrigger {
 public:
  MantaTrigger(uint16_t period_ms_, uint16_t width_ms_, GPIO_TypeDef* port_,
               uint16_t pin_)
      : _period_ms(period_ms_),
        _width_ms(width_ms_),
        _ctr(period_ms_),
        _state(Low),
        _port(port_),
        _pin(pin_){};

  inline uint8_t spin(void) {
    uint8_t rising_edge_step = 0;
    if (_state == Low) {
      // Wait for _ctr to reach 0
      if (!_ctr) {
        _state = High;
        // Set pin High
        rising_edge_step = 1;
        HAL_GPIO_WritePin(_port, _pin, GPIO_PIN_SET);
        _ctr = _width_ms;
      }
    }
    if (_state == High) {
      if (!_ctr) {
        _state = Low;
        // Set pin Low
        HAL_GPIO_WritePin(_port, _pin, GPIO_PIN_RESET);
        _ctr = _period_ms;
      }
    }
    _ctr--;
    return rising_edge_step;
  }

 protected:
  uint16_t _period_ms, _width_ms, _ctr;
  enum State { Low = 0, High = 1 };
  State _state = Low;
  GPIO_TypeDef* _port;
  uint16_t _pin;
};