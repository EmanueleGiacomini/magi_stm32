/**
 * @file fw_main.cpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2022-05-20
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "fw_main.h"

#include <cstdio>

#include "encoder.hpp"
#include "encoder_packets.h"
#include "packet_parser.h"
#include "packet_traits.h"
#include "system.h"

static PacketParser packet_parser;
static PacketTraits_<encoder::PacketEncoder, 1> traits_encoder(packet_parser);
static encoder::PacketEncoder packet_enc;

static uint16_t time_seq = 0;
constexpr uint16_t LoopPeriod = 10;  // 10 ms period
static uint16_t loop_iter = 0;

static AbsoluteEncoder enc_abs(ENC_DATA_GPIO_Port, ENC_DATA_Pin, GPIO_PIN_10,
                               &htim10);
static IncrementalEncoder enc_incr(&htim3);
static MantaTrigger manta_trigger(32, 1, MANTA_TRIGGER_GPIO_Port,
                                  MANTA_TRIGGER_Pin);  // 29 ms

void fw_main(void) {
  // Setup packet_parser
  packet_parser.setTxCallback([&](uint8_t tx_c) {
    return !HAL_UART_Transmit(&huart2, &tx_c, 1, HAL_MAX_DELAY);
  });

  enc_abs.set_protocol_start_cb([&](void) {
    enc_incr.sample();
    manta_trigger.spin();
  });

  // Start Timer11 for system loop
  // HAL_TIM_Base_Start_IT(&htim11);

  // TEST ENCODER
  HAL_TIM_Base_Start(&htim10);
  TIM2->CCR2 = 4;
  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);

  char buf[32];
  while (true) {
    // int wb = sprintf(buf, "%d\n", htim2.Instance->CNT);
    // HAL_UART_Transmit(&huart2, (uint8_t*)buf, wb, HAL_MAX_DELAY);
    // HAL_Delay(1);
    if (enc_abs.ready()) {
      enc_abs.ready() = false;
      int wb = sprintf(buf, "%d\n", enc_abs.sampled_value());
      HAL_UART_Transmit(&huart2, (uint8_t*)buf, wb, HAL_MAX_DELAY);
      // packet_enc.measurements[0] = enc_abs.sampled_value();
      // packet_enc.measurements[1] = enc_incr.sampled_value();
      // packet_enc.camera_sync = manta_trigger.spin();
      // packet_parser.sendPacket(packet_enc, 0);
    }
    enc_abs.spin();
    // if (!time_seq) {
    //   // packet_parser.sendPacket(encoder_manager.data(), 0);
    //   loop_iter++;
    // }
  }

  return;
}

// void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
//   if (htim == &htim11) {
//     encoder_manager.data().camera_sync = manta_trigger.spin();
//     time_seq = (time_seq + 1) % LoopPeriod;
//   }
// }

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) { enc_abs.proc_isr(GPIO_Pin); }
// TODO