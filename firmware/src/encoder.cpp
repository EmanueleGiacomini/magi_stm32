/**
 * @file encoder.cpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2022-05-20
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "encoder.hpp"

#include "fw_main.h"

AbsoluteEncoder::AbsoluteEncoder(GPIO_TypeDef *port_, uint16_t pin_,
                                 uint16_t exti_line_, TIM_HandleTypeDef *htim_)
    : _port(port_), _pin(pin_), _exti_line(exti_line_), _htim(htim_) {
  _inter_value = 0;
  HAL_TIM_Base_Start(_htim);
}
void AbsoluteEncoder::sample(void) { return; }

void AbsoluteEncoder::proc_isr(uint16_t exti_) {
  // if (exti_ == _exti_line) {
  // Reset TIM counter
  _htim->Instance->CNT = 0;
  HAL_GPIO_WritePin(DATA_DEBUG_GPIO_Port, DATA_DEBUG_Pin, GPIO_PIN_SET);
  // Read data pin
  uint8_t data = (_port->IDR & _pin) != GPIO_PIN_RESET;

  // Valid EXTI line triggered on falling edge

  if (_bit_idx == 0) {
    _inter_value = 0;
  } else {
    // Set [12 - (_bit_idx-1)]th bit in _inter_value
    _inter_value |= data << (12 - (_bit_idx - 1));
  }
  _bit_idx++;
  if (_bit_idx == 13) {
    _ready = true;
    _sampled_value = _inter_value;
    _bit_idx = 0;
    // Launch protocol_start_cb
    // if (_protocol_start_cb != nullptr) _protocol_start_cb();
  }
  HAL_GPIO_WritePin(DATA_DEBUG_GPIO_Port, DATA_DEBUG_Pin, GPIO_PIN_RESET);

  //}
}

void AbsoluteEncoder::spin(void) {
  if (_htim->Instance->CNT > 3000) {
    _bit_idx = 0;
    _inter_value = 0;
    _htim->Instance->CNT = 0;
  }
}

IncrementalEncoder::IncrementalEncoder(TIM_HandleTypeDef *htim_)
    : _htim(htim_) {}
void IncrementalEncoder::sample(void) {
  _sampled_value = _htim->Instance->CNT;
  return;
}