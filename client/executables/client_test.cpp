#include <encoder_packets.h>

#include <client.hpp>

#include "manta_listener.hpp"

void encoder_packet_callback(encoder::PacketEncoder* packet) {
  std::cout << packet->measurements[0] << std::endl;
}

const char* cam0_id = "000F314CE570";
const char* cam1_id = "000F314CE573";

int main(int argc, char* argv[]) {
  // Setup Manta cameras
  MantaListener cam0_listener(cam0_id, 3);
  MantaListener cam1_listener(cam1_id, 3);

  Client client("/dev/ttyACM0", 115200);
  PacketTraitsEncoder encoder_traits(*(client.getParser()), client);
  client.installCallback(encoder_packet_callback);

  while (true) {
    /* code */
  }

  return 0;
}