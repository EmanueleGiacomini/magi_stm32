#include "client.hpp"

Client::Client(std::string serial_device_, int baudrate_) {
  _uart.init(serial_device_, baudrate_);

  _thread_alive = std::make_shared<bool>(true);
  _receiver_thread = std::thread(&Client::receiverFun, this);
  _processor_thread = std::thread(&Client::queueProcessFun, this);
}

void Client::receiverFun() {
  std::cerr << "RECEIVER THREAD STARTED" << std::endl;
  char rx_buf = 0;
  while (*_thread_alive) {
    _uart.receive((uint8_t*)&rx_buf, 1);
    _packet_parser.parseChar(rx_buf);
  }
  return;
}

void Client::queueProcessFun() {
  std::cerr << "PACKET PROCESSOR THREAD STARTED" << std::endl;
  while (*_thread_alive) {
    if (!_encoder_queue.empty()) {
      encoder::PacketEncoder* packet  = _encoder_queue.front();
      _encoder_queue.pop();
      _encoder_callback(packet);
    }
  }
  return;
}

void Client::close() {
  *_thread_alive = false;
  _receiver_thread.join();
  _processor_thread.join();
  _uart.cclose();
}

