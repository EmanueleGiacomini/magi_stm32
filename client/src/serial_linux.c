#include "serial_linux.h"
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int serial_set_interface_attribs(int fd_, int speed_, int parity_) {
  struct termios tty;
  memset (&tty, 0, sizeof tty);

  if (tcgetattr (fd_, &tty) != 0) {
    printf ("error %d from tcgetattr", errno);
    return -1;
  }

  switch (speed_) {
  case 9600:
    speed_ = B9600;
    break;
  case 19200:
    speed_ = B19200;
    break;
  case 57600:
    speed_ = B57600;
    break;
  case 115200:
    speed_ = B115200;
    break;
  case 230400:
    speed_ = B230400;
    break;
  case 576000:
    speed_ = B576000;
    break;
  case 921600:
    speed_ = B921600;
    break;
  default:
    printf("cannot sed baudrate %d\n", speed_);
    return -1;
  }

  cfsetospeed (&tty, speed_);
  cfsetispeed (&tty, speed_);
  cfmakeraw(&tty);

  // enable reading
  tty.c_cflag &= ~(PARENB | PARODD);               // shut off parity
  tty.c_cflag |= parity_;
  tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;      // 8-bit chars

  if (tcsetattr (fd_, TCSANOW, &tty) != 0) {
    printf ("error %d from tcsetattr", errno);
    return -1;
  }

  return 0;
}

void serial_set_blocking(int fd_, int should_block_) {
  struct termios tty;
  memset (&tty, 0, sizeof tty);

  if (tcgetattr (fd_, &tty) != 0) {
      printf ("error %d from tggetattr", errno);
      return;
  }

  tty.c_cc[VMIN]  = should_block_ ? 1 : 0;
  tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

  if (tcsetattr (fd_, TCSANOW, &tty) != 0)
    printf ("error %d setting term attributes", errno);
}


int serial_open(const char* name_) {
  int fd = open (name_, O_RDWR | O_NOCTTY | O_SYNC );

  if (fd < 0) {
    printf ("error %d opening serial, fd %d\n", errno, fd);
  }
  
  return fd;
}
