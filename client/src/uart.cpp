#include <uart.hpp>
#include <iostream>

bool UART::init(const std::string& device_, int baudrate_) {
    if (_uart_fd > 0) {
        close(_uart_fd);
        _uart_fd=-1;
    }

    _uart_fd = serial_open(device_.c_str());

    if (_uart_fd < 0)
        return false;

    if (serial_set_interface_attribs(_uart_fd, baudrate_, 0) ) {
        close(_uart_fd);
        _uart_fd = -1;
        return false;
    }

    serial_set_blocking(_uart_fd, 1);

    return true;
}

bool UART::receive(uint8_t* buffer_, uint32_t size_) {
    memset(buffer_, 0, size_);

    uint32_t byte_read = read(_uart_fd, buffer_, size_);

    if (byte_read < size_)
        return false;
    return true;
}

bool UART::send(const uint8_t* buffer_, uint32_t size_) {
    uint32_t byte_written = write(_uart_fd, buffer_, size_);
    
    if (byte_written < size_)
        return false;
    return true;
}

void UART::cclose() {
    close(_uart_fd);
}
