#include "../include/manta_listener.hpp"

static int cameras_running = 0;

MantaListener::MantaListener(const char *camera_id_, size_t buffer_images_size_)
    : _camera_id(camera_id_), _frames_buffer(buffer_images_size_) {
  if (VmbErrorSuccess == _system.Startup())
    if (VmbErrorSuccess ==
        _system.OpenCameraByID(_camera_id, VmbAccessModeFull, _camera))
      std::cerr << "Camera with ID: " << _camera_id << " opened" << std::endl;
  cameras_running++;
}

MantaListener::~MantaListener() {
  if (_camera_running)
    stopAcquiring();

  if (VmbErrorSuccess == _camera->Close())
    std::cerr << "Camera with ID: " << _camera_id << " closed" << std::endl;

  cameras_running--;

  if (!cameras_running && VmbErrorSuccess == _system.Shutdown())
    std::cerr << "System shutting down" << std::endl;
}

void MantaListener::setParams(string XMLFile_) {
  VmbFeaturePersistSettings_t settingsStruct;
  settingsStruct.loggingLevel = 1;
  settingsStruct.maxIterations = 5;
  settingsStruct.persistType = VmbFeaturePersistNoLUT;

  if (VmbErrorSuccess != _camera->LoadCameraSettings(XMLFile_, &settingsStruct))
    std::cerr << "Error in setting camera parameters" << std::endl;
  else
    std::cerr << "Parameters setting done :)" << std::endl;
}

void MantaListener::startAcquiring() {
  Vimba::FeaturePtr feature;
  VmbInt64_t payload_size;

  _camera->GetFeatureByName("PayloadSize", feature);
  feature->GetValue(payload_size);

  for (auto &frame : _frames_buffer) {
    frame.reset(new Vimba::Frame(payload_size));
    frame->RegisterObserver(_observer);
    _camera->AnnounceFrame(frame);
  }

  _camera->StartCapture();

  for (const auto &frame : _frames_buffer) {
    _camera->QueueFrame(frame);
  }

  _camera->GetFeatureByName("AcquisitionStart", feature);
  feature->RunCommand();
  _camera_running = true;
}

void MantaListener::stopAcquiring() {
  Vimba::FeaturePtr feature;
  _camera->GetFeatureByName("AcquisitionStop", feature);
  feature->RunCommand();
  _camera->EndCapture();
  _camera->FlushQueue();
  _camera->RevokeAllFrames();
  _camera_running = false;
}

void MantaListener::setCallback(CallbackType callback_) {
  Vimba::IFrameObserverPtr observer(new FrameObserver(_camera, callback_));
  _observer = observer;
}