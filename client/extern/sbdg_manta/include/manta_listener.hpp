#pragma once

#include <VimbaCPP/Include/VimbaCPP.h>

#include <functional>
#include <iostream>

namespace Vimba = AVT::VmbAPI;

using CallbackType = std::function<void(const Vimba::FramePtr)>;
using string = std::string;

class MantaListener {
 private:
  class FrameObserver : public Vimba::IFrameObserver {
   public:
    inline FrameObserver(Vimba::CameraPtr camera_, CallbackType callback_)
        : IFrameObserver(camera_), _callback(callback_){};
    inline void FrameReceived(const Vimba::FramePtr frame_) {
      VmbFrameStatusType rec_status;
      if (VmbErrorSuccess == frame_->GetReceiveStatus(rec_status)) {
        if (VmbFrameStatusComplete == rec_status)
          _callback(frame_);
        else
          std::cerr << "falied to acquire an image!";
      }
      m_pCamera->QueueFrame(frame_);
    };

   private:
    CallbackType _callback;
  };

 public:
  MantaListener(const char* camera_id_, size_t buffer_images_size_ = 3);
  ~MantaListener();
  void setParams(string XMLFile_);
  void setCallback(CallbackType callback_);
  void startAcquiring();
  void stopAcquiring();

 protected:
  Vimba::VimbaSystem& _system = Vimba::VimbaSystem::GetInstance();
  Vimba::CameraPtr _camera;
  const char* _camera_id;
  Vimba::FramePtrVector _frames_buffer;
  Vimba::IFrameObserverPtr _observer;
  bool _camera_running = false;
};