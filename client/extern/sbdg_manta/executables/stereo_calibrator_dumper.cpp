/**
 * @file stereo_calibrator_dumper.cpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2022-03-08
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "manta_listener.hpp"
#include <iostream>
#include <opencv4/opencv2/highgui.hpp>
#include <opencv4/opencv2/opencv.hpp>

using String = std::string;

const String camera_0_params = "../config/DEV_000F314CE575_CALIBRATION.xml";
const String camera_1_params = "../config/DEV_000F314CE571_CALIBRATION.xml";

const String camera_id0 = "000F314CE575"; // RIGHT CAMERA
const String camera_id1 = "000F314CE571"; // LEFT CAMERA

const String image_r_folder = "cam0/";
const String image_l_folder = "cam1/";

String dump_path = "";

const String cv_wname = "Stereo view";

cv::Mat image_r, image_l;
bool r_ready = false, l_ready = false;
unsigned long image_saved_idx = 0;

void process_stereo() {
  if (r_ready && l_ready) {
    r_ready = false;
    l_ready = false;
    cv::Mat plot0_image, plot1_image;
    cv::resize(image_r, plot0_image, cv::Size(), 0.5, 0.5, cv::INTER_LINEAR);
    cv::resize(image_l, plot1_image, cv::Size(), 0.5, 0.5, cv::INTER_LINEAR);
    cv::hconcat(plot0_image, plot1_image, plot0_image);
    cv::imshow(cv_wname, plot0_image);
    uint16_t c = cv::waitKey(10);
    if (c == 32) {
      char temp_buf[32] = {'\0'};
      sprintf(temp_buf, "%04ld", image_saved_idx);
      String r_path = dump_path + image_r_folder + String(temp_buf) + ".png";
      String l_path = dump_path + image_l_folder + String(temp_buf) + ".png";
      cv::imwrite(r_path, image_r);
      cv::imwrite(l_path, image_l);
      std::cerr << "Image Right saved at " << r_path << std::endl;
      std::cerr << "Image Left saved at " << l_path << std::endl;
      image_saved_idx++;
    }
  }
}

int main(int argc, char **argv) {

  if (argc == 2) {
    dump_path = argv[1];
    std::cerr << "Destination path=" << dump_path << std::endl;
  } else {
    std::cerr << "Specify destination path.\n";
    return -1;
  }

  MantaListener camera_right(camera_id0.c_str(), 5);
  MantaListener camera_left(camera_id1.c_str(), 5);

  camera_right.setParams(camera_0_params);
  camera_left.setParams(camera_1_params);

  CallbackType cb_right = [&](const Vimba::FramePtr frame) {
    uint32_t rows, cols;
    frame->GetHeight(rows);
    frame->GetWidth(cols);
    u_char *image_buf;
    frame->GetBuffer(image_buf);
    cv::Mat cimage(rows, cols, CV_8UC1, image_buf);
    image_r = cimage;
    r_ready = true;
    process_stereo();
  };

  CallbackType cb_left = [&](const Vimba::FramePtr frame) {
    uint32_t rows, cols;
    frame->GetHeight(rows);
    frame->GetWidth(cols);
    u_char *image_buf;
    frame->GetBuffer(image_buf);
    cv::Mat cimage(rows, cols, CV_8UC1, image_buf);
    image_l = cimage;
    l_ready = true;
    process_stereo();
  };

  camera_right.setCallback(cb_right);
  camera_left.setCallback(cb_left);

  camera_right.startAcquiring();
  camera_left.startAcquiring();

  while (true) {
  }

  return 0;
}