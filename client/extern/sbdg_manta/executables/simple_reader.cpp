#include "../include/manta_listener.hpp"
#include <opencv2/opencv.hpp>
#include <chrono>
#include <unistd.h>
#include <cstdlib>
#include <signal.h>

const std::string camera0_params = "../config/DEV_000F314CE575.xml";
const std::string camera1_params = "../config/DEV_000F314CE571.xml";
const std::string window_name0 = "manta image acquired 0";
const std::string window_name1 = "manta image acquired 1";

static bool running = true;

void ctrc_handler(int s) {
    running = false;
};

int main ( int argc, char* argv[] ) {
    std::string camera_id0("000F314CE575");
    std::string camera_id1("000F314CE571");

    MantaListener camera0(camera_id0.c_str());
    MantaListener camera1(camera_id1.c_str());

    camera0.setParams(camera0_params);
    camera1.setParams(camera1_params);

    CallbackType callback0 = [&] (const Vimba::FramePtr frame_) {
        uint32_t rows, cols;
        frame_->GetHeight(rows);
        frame_->GetWidth(cols);
        u_char* buffer;
        frame_->GetBuffer(buffer);
        cv::Mat cv_image(rows, cols, CV_8UC1, buffer);
        cv::imshow(window_name0, cv_image); 
    };
    CallbackType callback1 = [&] (const Vimba::FramePtr frame_) {
        uint32_t rows, cols;
        frame_->GetHeight(rows);
        frame_->GetWidth(cols);
        u_char* buffer;
        frame_->GetBuffer(buffer);
        cv::Mat cv_image(rows, cols, CV_8UC1, buffer);
        cv::imshow(window_name1, cv_image);
    };

    camera0.setCallback(callback0);
    camera0.startAcquiring();
    camera1.setCallback(callback1);
    camera1.startAcquiring();
    
    cv::namedWindow(window_name0);
    cv::namedWindow(window_name1);

    struct sigaction sig_int_handler;

    sig_int_handler.sa_handler = ctrc_handler;
    sigemptyset(&sig_int_handler.sa_mask);
    sig_int_handler.sa_flags = 0;

    sigaction(SIGINT, &sig_int_handler, NULL);

    while(running) {
        //auto start = std::chrono::high_resolution_clock::now();
        //camera0.getImage('c');
        //auto finish = std::chrono::high_resolution_clock::now();
        //std::cerr << std::chrono::duration_cast<std::chrono::milliseconds>(finish - start).count() << std::endl;
        //camera1.getImage('1');
        //cv::Mat image = camera0.getCvImage();
        
        //cv::imshow(window_name, image);
        cv::waitKey(1);
        //usleep(1000);
    }
    

    return 0;
}