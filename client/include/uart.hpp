#pragma once
#include <unistd.h>
#include <fcntl.h>
#include <iostream>
#include <string.h>
#include "serial_linux.h"

class UART {

  public:

    bool init(const std::string& device_, int baudrate_);

    bool receive(uint8_t* buffer_, uint32_t size_);

    bool send(const uint8_t* buffer_, uint32_t size_);

    void cclose();

  protected:
    int _uart_fd = -1;
};