#pragma once

#include <counting_semaphore.h>
#include <unistd.h>

#include <circular_buffer.hpp>
#include <packet_parser.h>
#include <packet_traits.h>
#include <functional>
#include <iostream>
#include <memory>
#include <mutex>
#include <encoder_packets.h>
#include <queue>
#include <thread>
#include <uart.hpp>

class Client {
 public:
  using EncoderCallBack = std::function<void(encoder::PacketEncoder*)>;

  Client(std::string serial_device_, int baudrate_);
  Client(const Client&) = delete;

  inline void installCallback(EncoderCallBack encoder_callback_) {
    _encoder_callback = encoder_callback_;
    return;
  }

  PacketParser* getParser(void) { return &_packet_parser; }

  void close();

 protected:
  UART _uart;
  PacketParser _packet_parser;

  std::shared_ptr<bool> _thread_alive;
  std::thread _receiver_thread;
  std::thread _processor_thread;

  std::queue<encoder::PacketEncoder*> _encoder_queue;

  EncoderCallBack _encoder_callback;

  void receiverFun();
  void queueProcessFun();

 friend class PacketTraitsEncoder;
};

struct PacketTraitsEncoder: public PacketTraits_<encoder::PacketEncoder, 1> {
  PacketTraitsEncoder(PacketParser& parser_, Client &client_): PacketTraits_<encoder::PacketEncoder, 1>(parser_), _client(client_){}

  void rxComplete(PacketIndex idx, PacketSeq seq) override {
    PacketTraits_<encoder::PacketEncoder, 1>::rxComplete(idx, seq);
    _client._encoder_queue.push((encoder::PacketEncoder*) &_temp_buffer);
  }

  Client &_client;
};