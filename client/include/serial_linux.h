#pragma once
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>

#ifdef __cplusplus
extern "C" {
#endif
  
  //! returns the descriptor of a serial port
  int serial_open(const char* name_);

  //! sets the attributes
  int serial_set_interface_attribs(int fd_, int speed_, int parity_);
  
  //! puts the port in blocking/nonblocking mode
  void serial_set_blocking(int fd_, int should_block_);

#ifdef __cplusplus
}
#endif
